"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUsersInRoom = exports.getUser = exports.removeUser = exports.addUser = void 0;
const users = [];
const addUser = ({ id, name, room, }) => {
    name = name.trim().toLocaleLowerCase();
    room = name.trim().toLocaleLowerCase();
    const existingUser = users.find((user) => user.room === room && user.name === name);
    if (existingUser)
        return { error: "Username is taken" };
    const user = { id, name, room };
    users.push(user);
    return { user };
};
exports.addUser = addUser;
const removeUser = (id) => {
    const index = users.findIndex((u) => u.id === id);
    if (index !== -1)
        return users.splice(index, 1)[0];
    return { error: "user not fund" };
};
exports.removeUser = removeUser;
const getUser = (id) => users.find((u) => u.id === id);
exports.getUser = getUser;
const getUsersInRoom = (room) => users.filter((u) => u.room === room);
exports.getUsersInRoom = getUsersInRoom;
//# sourceMappingURL=users.js.map