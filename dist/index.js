"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const connect_redis_1 = __importDefault(require("connect-redis"));
const cors_1 = __importDefault(require("cors"));
require("dotenv-safe/config");
const express_1 = __importDefault(require("express"));
const express_session_1 = __importDefault(require("express-session"));
const http_1 = require("http");
const ioredis_1 = __importDefault(require("ioredis"));
require("reflect-metadata");
const socket_io_1 = require("socket.io");
const type_graphql_1 = require("type-graphql");
const general_1 = require("./constants/general");
const hello_1 = require("./resolvers/hello");
const user_1 = require("./resolvers/user");
const router_1 = __importDefault(require("./router"));
const uuid_1 = require("uuid");
const client_1 = require("@prisma/client");
const port = process.env.PORT || 4000;
const main = () => __awaiter(void 0, void 0, void 0, function* () {
    const app = express_1.default();
    const prisma = new client_1.PrismaClient();
    const RedisStore = connect_redis_1.default(express_session_1.default);
    const redis = new ioredis_1.default(process.env.REDIS_URL);
    app.set("trust proxy", 1);
    app.use(cors_1.default({
        origin: process.env.CORS_ORIGIN,
        credentials: true,
    }));
    app.use(express_session_1.default({
        name: general_1.COOKIE_NAME,
        store: new RedisStore({
            client: redis,
            disableTouch: true,
        }),
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 365 * 10,
            httpOnly: true,
            sameSite: "lax",
            secure: general_1.__prod__,
            domain: general_1.__prod__ ? ".belzaondrej.com" : "",
        },
        saveUninitialized: false,
        secret: process.env.SESSION_SECRET,
        resave: false,
    }));
    const server = http_1.createServer(app);
    const io = new socket_io_1.Server(server);
    io.on("connection", (socket) => {
        console.log("user has connected");
        socket.on("joinRoom", ({ room, userId }) => __awaiter(void 0, void 0, void 0, function* () {
            //Finds user in db
            const user = yield prisma.user.findFirst({ where: { id: userId } });
            //if user is found sets relation to redis for quick access
            if (!user)
                return;
            redis.set(socket.id, user.id);
            //Finds room which user want to connect and loads messages from that room
            const dbRoom = yield prisma.room.findFirst({
                where: {
                    name: "general",
                },
                include: {
                    messages: {
                        include: {
                            user: true,
                        },
                    },
                },
            });
            //check if room has been found
            if (!(dbRoom === null || dbRoom === void 0 ? void 0 : dbRoom.messages))
                return;
            const messages = dbRoom.messages;
            //Maps messages from db to application format
            const mappedMessages = messages.map((m) => ({
                user: m.user.username,
                message: m.message,
                userId: m.userId,
                id: m.id,
            }));
            //send loaded messages to user
            socket.emit("loadMessages", [
                ...mappedMessages,
                {
                    user: "server",
                    message: `Welcome back`,
                    userId: user === null || user === void 0 ? void 0 : user.id,
                    id: uuid_1.v4(),
                },
            ]);
            //send message to other users that new user has connected to chanel
            socket.broadcast.to(room).emit("message", {
                user: "server",
                message: `${user === null || user === void 0 ? void 0 : user.username} user has joined `,
                userId: user === null || user === void 0 ? void 0 : user.id,
                id: uuid_1.v4(),
            });
            socket.join(room);
        }));
        socket.on("sendMessage", (message, callback) => __awaiter(void 0, void 0, void 0, function* () {
            //Loads user id from redis db
            const userId = yield redis.get(socket.id);
            if (!userId)
                return;
            //finds user in postgresSql
            const user = yield prisma.user.findFirst({
                where: { id: parseInt(userId) },
            });
            //finds current room
            const room = yield prisma.room.findFirst({
                where: { name: "general" },
            });
            //adds new message to db
            if (!user || !room)
                return;
            const dbMessage = yield prisma.message.create({
                data: {
                    message: message,
                    userId: user.id,
                    roomId: room.id,
                },
            });
            //Emits new message to all connected users
            io.to("general").emit("message", {
                user: user.username,
                message: message,
                userId: user.id,
                id: dbMessage.id,
            });
            callback();
        }));
        socket.on("disconnect", () => __awaiter(void 0, void 0, void 0, function* () {
            //Clears value from redis
            yield redis.del(socket.id);
            console.log("user had disconnected");
        }));
    });
    const apolloServer = new apollo_server_express_1.ApolloServer({
        schema: yield type_graphql_1.buildSchema({
            resolvers: [hello_1.HelloResolver, user_1.UserResolver],
            validate: false,
        }),
        context: ({ req, res }) => ({
            req,
            res,
            redis,
        }),
    });
    apolloServer.applyMiddleware({
        app,
        cors: false,
    });
    app.use(router_1.default);
    server.listen(port, () => {
        console.log(`listening on *:${port}`);
    });
});
main().catch((err) => console.error(err));
//# sourceMappingURL=index.js.map