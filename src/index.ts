import { ApolloServer } from "apollo-server-express";
import connectRedis from "connect-redis";
import cors from "cors";
import "dotenv-safe/config";
import express from "express";
import session from "express-session";
import { createServer } from "http";
import Redis from "ioredis";
import "reflect-metadata";
import { Server, Socket } from "socket.io";
import { buildSchema } from "type-graphql";
import { COOKIE_NAME, __prod__ } from "./constants/general";
import { HelloResolver } from "./resolvers/hello";
import { UserResolver } from "./resolvers/user";
import router from "./router";
import { v4 as uuidv4 } from "uuid";
import { PrismaClient } from "@prisma/client";

const port = process.env.PORT || 4000;

const main = async () => {
  const app = express();

  const prisma = new PrismaClient();

  const RedisStore = connectRedis(session);
  const redis = new Redis(process.env.REDIS_URL);

  app.set("trust proxy", 1);
  app.use(
    cors({
      origin: process.env.CORS_ORIGIN,
      credentials: true,
    })
  );

  app.use(
    session({
      name: COOKIE_NAME,
      store: new RedisStore({
        client: redis,
        disableTouch: true,
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, //10 years
        httpOnly: true,
        sameSite: "lax",
        secure: __prod__,
        domain: __prod__ ? ".belzaondrej.com" : "",
      },
      saveUninitialized: false,
      secret: process.env.SESSION_SECRET,
      resave: false,
    })
  );

  const server = createServer(app);
  const io = new Server(server);

  io.on("connection", (socket: Socket) => {
    console.log("user has connected");
    socket.on(
      "joinRoom",
      async ({ room, userId }: { room: string; userId: number }) => {
        //Finds user in db
        const user = await prisma.user.findFirst({ where: { id: userId } });

        //if user is found sets relation to redis for quick access
        if (!user) return;
        redis.set(socket.id, user.id);

        //Finds room which user want to connect and loads messages from that room
        const dbRoom = await prisma.room.findFirst({
          where: {
            name: "general",
          },
          include: {
            messages: {
              include: {
                user: true,
              },
            },
          },
        });

        //check if room has been found
        if (!dbRoom?.messages) return;
        const messages = dbRoom.messages;

        //Maps messages from db to application format
        const mappedMessages = messages.map((m) => ({
          user: m.user.username,
          message: m.message,
          userId: m.userId,
          id: m.id,
        }));

        //send loaded messages to user
        socket.emit("loadMessages", [
          ...mappedMessages,
          {
            user: "server",
            message: `Welcome back`,
            userId: user?.id,
            id: uuidv4(),
          },
        ]);

        //send message to other users that new user has connected to chanel
        socket.broadcast.to(room).emit("message", {
          user: "server",
          message: `${user?.username} user has joined `,
          userId: user?.id,
          id: uuidv4(),
        });

        socket.join(room);
      }
    );

    socket.on("sendMessage", async (message: string, callback: () => void) => {
      //Loads user id from redis db
      const userId = await redis.get(socket.id);
      if (!userId) return;

      //finds user in postgresSql
      const user = await prisma.user.findFirst({
        where: { id: parseInt(userId) },
      });

      //finds current room
      const room = await prisma.room.findFirst({
        where: { name: "general" },
      });

      //adds new message to db
      if (!user || !room) return;
      const dbMessage = await prisma.message.create({
        data: {
          message: message,
          userId: user.id,
          roomId: room.id,
        },
      });

      //Emits new message to all connected users
      io.to("general").emit("message", {
        user: user.username,
        message: message,
        userId: user.id,
        id: dbMessage.id,
      });
      callback();
    });

    socket.on("disconnect", async () => {
      //Clears value from redis
      await redis.del(socket.id);
      console.log("user had disconnected");
    });
  });

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [HelloResolver, UserResolver],
      validate: false,
    }),
    context: ({ req, res }) => ({
      req,
      res,
      redis,
    }),
  });

  apolloServer.applyMiddleware({
    app,
    cors: false,
  });

  app.use(router);

  server.listen(port, () => {
    console.log(`listening on *:${port}`);
  });
};

main().catch((err) => console.error(err));
