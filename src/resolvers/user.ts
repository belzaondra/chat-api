import { PrismaClient } from "@prisma/client";
import {
  Arg,
  Ctx,
  Field,
  Mutation,
  ObjectType,
  Query,
  Resolver,
} from "type-graphql";
import { MyContext } from "../types";
import { UsernamePasswordEmailInput } from "./Inputs/UsernamePasswordEmailInput";
import argon2 from "argon2";
import { COOKIE_NAME } from "../constants/general";

@ObjectType()
class FiledError {
  @Field()
  field!: string;

  @Field()
  message!: string;
}

@ObjectType()
class User {
  @Field()
  email!: string;

  @Field()
  username!: string;

  @Field()
  id!: number;
}

@ObjectType()
class UserResponse {
  @Field(() => [FiledError], { nullable: true })
  errors?: FiledError[];

  @Field(() => User, { nullable: true })
  user?: User;
}

const prisma = new PrismaClient();

@Resolver(User)
export class UserResolver {
  @Query(() => String)
  async users() {
    console.log(await prisma.user.findMany());
    return "Hello";
  }

  @Query(() => User, { nullable: true })
  async me(@Ctx() { req }: MyContext): Promise<User | null> {
    if (!req.session.userId) return null;
    const user = await prisma.user.findFirst({
      where: { id: req.session.userId },
    });
    return user;
  }

  @Mutation(() => UserResponse)
  async register(
    @Arg("options") options: UsernamePasswordEmailInput,
    @Ctx() { req }: MyContext
  ): Promise<UserResponse> {
    const hashedPassword = await argon2.hash(options.password);
    let user;

    user = await prisma.user.create({
      data: {
        email: options.email,
        password: hashedPassword,
        username: options.username,
      },
    });

    //Login user
    req.session.userId = user.id;
    return { user };
  }

  @Mutation(() => UserResponse, { nullable: true })
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() { req }: MyContext
  ): Promise<UserResponse> {
    const user = await prisma.user.findFirst({
      where: {
        email: email,
      },
    });
    if (!user)
      return {
        errors: [
          {
            field: "usernameOrEmail",
            message: "Username or email doesn't exist",
          },
        ],
      };

    const isValid = await argon2.verify(user.password, password);
    if (!isValid)
      return {
        errors: [{ field: "password", message: "Wrong password" }],
      };

    req.session.userId = user.id;
    return {
      user,
    };
  }

  @Mutation(() => Boolean)
  async logout(@Ctx() { req, res }: MyContext) {
    return new Promise((resolve) =>
      req.session.destroy((err) => {
        res.clearCookie(COOKIE_NAME);
        if (err) {
          resolve(false);
          return;
        } else resolve(true);
      })
    );
  }
}
