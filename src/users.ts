const users: user[] = [];
type user = {
  id: string;
  name: string;
  room: string;
};

export const addUser = ({
  id,
  name,
  room,
}: {
  id: string;
  name: string;
  room: string;
}) => {
  name = name.trim().toLocaleLowerCase();
  room = name.trim().toLocaleLowerCase();

  const existingUser = users.find(
    (user) => user.room === room && user.name === name
  );

  if (existingUser) return { error: "Username is taken" };

  const user: user = { id, name, room };
  users.push(user);
  return { user };
};

export const removeUser = (id: string) => {
  const index = users.findIndex((u) => u.id === id);
  if (index !== -1) return users.splice(index, 1)[0];
  return { error: "user not fund" };
};

export const getUser = (id: string) => users.find((u) => u.id === id);

export const getUsersInRoom = (room: string) =>
  users.filter((u) => u.room === room);
