import { Request, Response } from "express";
import { SessionData } from "express-session";
import { Redis } from "ioredis";
import { Session } from "node:inspector";
export type MyContext = {
  req: Request & {
    session: Session & Partial<SessionData> & { userId: number };
  };
  res: Response;
  redis: Redis;
};
